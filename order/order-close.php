<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Закрытые наряды</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="style.css?rt" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</head>
<body>
<!-- Контент -->
<div class="container">
    <article class="order-wrapper border-top border-bottom">
        <div class="row">
            <div class="col-11 order-col-more">
                <!-- Шапка наряда -->
                <section class="order-header border-bottom">
                        <div class="row">
                            <div class="col">
                                <div class="order-date"> <!-- дата создания наряда -->
                                    <span class="order-day">19</span>
                                    <div class="d-inline-block">
                                        <span class="order-month-year">10.2018</span>
                                        <span class="badge badge-secondary">Пятница</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col">
                                <div class="order-schedule text-right">
                                    <span>16:00</span>
                                    <span>сегодня</span>
                                </div>
                            </div>
                        </div>
                </section>

                <!-- Контент наряда -->
                <section class="order-body">
                            <div class="row">
                                <div class="col-9">
                                    <table class="table order-table">
                                        <tbody>
                                            <tr>
                                                <th><small class="text-muted">Адрес</small></th>
                                                <td><p style="margin: 0;" class="order-hidden-text">11-78-11</p></td>
                                            </tr>
                                            <tr class="pb-0">
                                                <th scope="row" class="pb-0"><small class="text-muted">Причины</small></th>
                                                <td class="order-table-font-size pb-0"><p class="order-hidden-text">Не получает IP</p></td>
                                                
                                            </tr>
                                            <tr class="px-2 py-0">
                                                <th scope="row" class="order-comment px-2 py-0"><small class="text-muted"></small></th>
                                                <td class="order-table-font-size order-comment px-2 py-0"><p class="order-hidden-text order-hidden-comment text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="pb-0"><small class="text-muted">Решения</small></th>
                                                <td class="order-table-font-size pb-0"><p class="order-hidden-text">Установили MAC</p></td>
                                            </tr>
                                            <tr class="order-comment-wrap">
                                                <th scope="row" class="order-comment px-2 py-0"><small class="text-muted"></small></th>
                                                <td class="order-table-font-size order-comment px-2 py-0"><p class="order-hidden-text order-hidden-comment text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><small class="text-muted">Исполнители</small></th>
                                                <td class="order-table-font-size"><p class="order-hidden-text">Климов Максим, Стрижак Татьяна</p></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><small class="text-muted">Замечания</small></th>
                                                <td class="order-table-font-size"><i class="fas fa-clock" data-toggle="tooltip" data-placement="top" title="На 30 минут"></i></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-3">
                                    <section class="order-time text-right">
                                        <span class="order-departure-duraction"><i class="fas fa-road" data-toggle="tooltip" data-placement="left" title="Время в дороге"></i> 0:23:16</span>
                                        <span class="order-delay"><i class="fas fa-play" data-toggle="tooltip" data-placement="left" title="Затраченное время на выполнение"></i> 1:29:23</span>
                                        <span class="order-full-time-duraction"><i class="fas fa-stopwatch" data-toggle="tooltip" data-placement="left" title="Общее время выполнения наряда"></i> 1:52:39</span>
                                    </section>
                                </div>
                            </div>
                        <div class="text-center">
                            <a href="#" class="btn btn-outline-success order-button-font-size">Подтвердить</a>
                            <a href="#" class="btn btn-outline-danger order-button-font-size" data-toggle="modal" data-target="#order-modal-cancel">Отклонить</a>
                            <!-- <a href="#" class="btn btn-outline-primary order-button-font-size">Подробнее</a> -->
                        </div>
                </section>
            </div>
            <a href="#" class="col-1 order-col-more bg-white" title="Подробнее">
                <table class="order-more-info">
                    <tbody>
                        <tr>
                        <td class="align-middle"><i class="fas fa-angle-right"></i></td>
                        </tr>
                    </tbody>
                </table>
            </a>
        </div>
    </article>

<br />
    <!-- Обстлуживание сети -->
    <article>
        <div class="row">
            <a href="#" class="col-3 order-service-network border-bottom-0 bg-white" title="Подробнее">
                <span class="order-service-network-address">11-78</span>
                <span class="text-truncate">Нет линка</span>
            </a>
            <a href="#" class="col-3 order-service-network bg-secondary text-white border-bottom-0" title="Подробнее">
                <span class="order-service-network-address">13-6</span>
                <span class="text-truncate">Настроить роутер</span>
            </a>
            <a href="#" class="col-3 order-service-network bg-success text-white" title="Подробнее">
                <span class="order-service-network-address">1-10</span>
                <span class="text-truncate">Замена кабеля</span>
            </a>
            <a href="#" class="col-3 order-service-network border-bottom-0 bg-white" title="Подробнее">
                <span class="order-service-network-address">33-20</span>
                <span class="text-truncate">Проверить кабель</span>
            </a>
            <a href="#" class="col-3 order-service-network bg-white border-top" title="Подробнее">
                <span class="order-service-network-address">11-32</span>
                <span class="text-truncate">Плохая скорость</span>
            </a>
            <a href="#" class="col-3 order-service-network bg-danger text-white border-top" title="Подробнее">
                <span class="order-service-network-address">26-15</span>
                <span class="text-truncate">Документы на подпись</span>
            </a>
            <a href="#" class="col-3 order-service-network bg-warning border-top" title="Подробнее">
                <span class="order-service-network-address">17-72</span>
                <span class="text-truncate">Субботник</span>
            </a>
            <a href="#" class="col-3 order-service-network bg-white border-top" title="Подробнее">
                <span class="order-service-network-address">11-78</span>
                <span class="text-truncate"></span>
            </a>
        </div>
    </article>
<br />

    <article class="order-wrapper border-top border-bottom">
        <!-- Шапка наряда -->
        <div class="row">
            <div class="col-11 order-col-more">
                <section class="order-header bg-warning border-bottom">
                        <div class="row">
                            <div class="col">
                                <div class="order-date">
                                    <span class="order-day">20</span>
                                    <div class="d-inline-block">
                                        <span class="order-month-year">10.2018</span>
                                        <span class="badge badge-danger">Суббота</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="order-schedule text-right">
                                    <span>17:30</span>
                                    <span>16.10.18</span>
                                </div>
                            </div>
                        </div>
                </section>
 
                <!-- Контент наряда -->
                <section class="order-body">
                    <section class=" ">
                        <div class="row">
                            <div class="col-9">
                                <table class="table order-table">
                                    <tbody>
                                        <tr>
                                            <th><small class="text-muted">Адрес</small></th>
                                            <td><p style="margin: 0;" class="order-hidden-text">11-78-11</p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="pb-0"><small class="text-muted">Причины</small></th>
                                            <td class="order-table-font-size pb-0"><p class="order-hidden-text">Плохая скорость</p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="order-comment px-2 py-0"><small class="text-muted"></small></th>
                                            <td class="order-table-font-size order-comment px-2 py-0"><p class="order-hidden-text order-hidden-comment text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="pb-0"><small class="text-muted">Решения</small></th>
                                            <td class="order-table-font-size pb-0"><p class="order-hidden-text">Проблема в оборудование абонента</p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="order-comment px-2 py-0"><small class="text-muted"></small></th>
                                            <td class="order-table-font-size order-comment px-2 py-0"><p class="order-hidden-text order-hidden-comment text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><small class="text-muted">Исполнители</small></th>
                                            <td class="order-table-font-size"><p class="order-hidden-text">Жидко Максим, Рыбаков Сергей</p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-3">
                                <section class="order-time text-right">
                                    <span class="order-departure-duraction"><i class="fas fa-road" data-toggle="tooltip" data-placement="left" title="Время в дороге"></i> 0:02:10</span>
                                    <span class="order-time text-right order-success"><i class="fas fa-play" data-toggle="tooltip" data-placement="left" title="Затраченное время на выполнение"></i> 0:10:15</span>
                                    <span class="order-full-time-duraction"><i class="fas fa-stopwatch" data-toggle="tooltip" data-placement="left" title="Общее время выполнения наряда"></i> 0:12:25</span>
                                </section>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="#" class="btn btn-outline-success order-button-font-size">Подтвердить</a>
                            <a href="#" class="btn btn-outline-danger order-button-font-size" data-toggle="modal" data-target="#order-modal-cancel">Отклонить</a>
                            <!-- <a href="#" class="btn btn-outline-primary order-button-font-size">Подробнее</a> -->
                        </div>
                    </section>
                </section>
            </div>
            <a href="#" class="col-1 order-col-more bg-white" title="Подробнее">
                <table class="order-more-info">
                    <tbody>
                        <tr>
                        <td class="align-middle"><i class="fas fa-angle-right"></i></td>
                        </tr>
                    </tbody>
                </table>
            </a>
        </div>
    </article>

    <article class="order-wrapper border-top border-bottom">
    <div class="row">
            <div class="col-11 order-col-more">
                <!-- Шапка наряда -->
                <section class="order-header border-bottom">
                        <div class="row">
                            <div class="col">
                                <div class="order-date">
                                    <span class="order-day">21</span>
                                    <div class="d-inline-block">
                                        <span class="order-month-year">10.2018</span>
                                        <span class="badge badge-danger">Воскресенье</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col">
                                <div class="order-schedule text-right">
                                    <span>10:05</span>
                                    <span>01.10.18</span>
                                </div>
                            </div>
                        </div>
                </section>

                <!-- Контент наряда -->
                <section class="order-body">
                        <div class="row">
                            <div class="col-9">
                                <table class="table order-table">
                                    <tbody>
                                        <tr>
                                            <th><small class="text-muted">Адрес</small></th>
                                            <td><p style="margin: 0;" class="order-hidden-text">11-78-11</p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="pb-0"><small class="text-muted">Причины</small></th>
                                            <td class="order-table-font-size pb-0"><p class="order-hidden-text">Поврежден кабель</p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="order-comment px-2 py-0"><small class="text-muted"></small></th>
                                            <td class="order-table-font-size order-comment px-2 py-0"><p class="order-hidden-text order-hidden-comment text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                        </tr>
                                        <tr class="order-no-pd-bottom">
                                            <th scope="row" class="pb-0"><small class="text-muted">Решения</small></th>
                                            <td class="order-table-font-size pb-0"><p class="order-hidden-text">Переобжали</p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="order-caption-table order-comment px-2 py-0"><small class="text-muted"></small></th>
                                            <td class="order-table-font-size order-comment px-2 py-0"><p class="order-hidden-text order-hidden-comment text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><small class="text-muted">Исполнители</small></th>
                                            <td class="order-table-font-size"><p class="order-hidden-text">Аврайцев Александр, Сташко Александр</p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><small class="text-muted">Замечания</small></th>
                                            <td class="order-table-font-size"><i class="fas fa-clock" data-toggle="tooltip" data-placement="top" title="На 40 минут"></i> <i class="fas fa-phone-slash" data-toggle="tooltip" data-placement="top" title="87077540021, 877824123483, 877560043234"></i></td>
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-3">
                                <section class="order-time text-right">
                                    <span class="order-departure-duraction"><i class="fas fa-road" data-toggle="tooltip" data-placement="left" title="Время в дороге"></i> 0:33:15</span>
                                    <span class="order-delay"><i class="fas fa-play" data-toggle="tooltip" data-placement="left" title="Затраченное время на выполнение"></i> 1:13:05</span>
                                    <span class="order-full-time-duraction"><i class="fas fa-stopwatch" data-toggle="tooltip" data-placement="left" title="Общее время выполнения наряда"></i> 1:46:20</span>
                                </section>
                            </div>
                        </div>
                    <div class="text-center">
                        <a href="#" class="btn btn-outline-success order-button-font-size">Подтвердить</a>
                        <a href="#" class="btn btn-outline-danger order-button-font-size" data-toggle="modal" data-target="#order-modal-cancel">Отклонить</a>
                        <!-- <a href="#" class="btn btn-outline-primary order-button-font-size">Подробнее</a> -->
                    </div>
                </section>
            </div>
            <a href="#" class="col-1 order-col-more bg-white" title="Подробнее">
                <table class="order-more-info">
                    <tbody>
                        <tr>
                        <td class="align-middle"><i class="fas fa-angle-right"></i></td>
                        </tr>
                    </tbody>
                </table>
            </a>
        </div>
    </article>
</div>








<!-- Модальное окно -->  
<div class="modal fade" id="order-modal-cancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Обоснуйте отклонение</h4>
        </div>
        <div class="modal-body">
            <h5>Введите причину отклонения:</h5>
            <textarea name="" id="" cols="45" rows="5"></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
            <button type="button" class="btn btn-primary">Отправить</button>
        </div>
        </div>
    </div>
</div>

<!-- Скрипты -->
<script type="text/javascript">
    //Тултип
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    //Модальное окно
    $('#order-modal-cancel').on('shown.bs.modal', function () {
        $('.order-cancel').focus()
    })
    
    //Поля выбора select
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });

    //Отрытие скрытого текста
    $('.order-hidden-text').on('click', function(){
        $(this).toggleClass('order-hidden-text-show');
    })  
</script>
</body>
</html>