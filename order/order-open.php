<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Открытые наряды</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="style.css?rt" />

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</head>
<body>
<!-- Контент -->
<div class="container">
<!-- Цикл -->
<?php For( $a = 0; $a < 10; $a++ ) { ?>

 <article class="order-wrapper border-top border-bottom">
        <div class="row">
            <div class="col-11 order-col-more">
                <!-- Шапка наряда -->
                <section class="order-header border-bottom">
                        <div class="row">
                            <div class="col">
                                <div class="order-date">
                                    <span class="order-day"><?=date("d",time()) ?></span>
                                    <div class="d-inline-block">
                                        <span class="order-month-year"><?=date("M",time()) ?></span>
                                        <span class="badge badge-secondary">Пятница</span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col">
                                <span class="order-address text-center">11-78-11</span>
                            </div> -->
                            <div class="col">
                                <div class="order-schedule text-right">
                                    <span><i class="fas fa-bell"></i> 16:00</span>
                                    <span>сегодня</span>
                                </div>
                            </div>
                        </div>
                </section>

                <!-- Контент наряда -->
                <section class="order-body">
                    <section class=" ">
                        <div class="row">
                            <div class="col-9">
                                <table class="table order-table">
                                    <tbody>
                                        <tr>
                                            <th><small class="text-muted">Адрес</small></th>
                                            <td><p style="margin: 0;" class="order-hidden-text">11-78-11</p></td>
                                        </tr>
                                        <tr class="order-no-pd-bottom">
                                            <th scope="row" class="pb-0"><small class="text-muted">Причины</small></th>
                                            <td class="order-table-font-size pb-0"><p class="order-hidden-text">Не получает IP</p></td>
                                        </tr>
                                        <tr class="order-comment-wrap">
                                            <th scope="row" class="order-comment px-2 py-0"><small class="text-muted"></small></th>
                                            <td class="order-table-font-size order-comment px-2 py-0"><p class="order-hidden-text order-hidden-comment text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                        </tr>
                                        
                                        <tr>
                                            <th scope="row" class="order-caption-table"><small class="text-muted">Исполнители</small></th>
                                            <td class="order-table-font-size"><p class="order-hidden-text"><i class="fas fa-pause"></i> Климов Максим, Стрижак Татьяна <i class="fas fa-car"></i></p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="order-caption-table order-comment"><small class="text-muted">Модификации</small></th>
                                            <td class="order-table-font-size order-comment"><p class="order-hidden-text order-hidden-comment text-muted" data-toggle="tooltip" data-placement="top" title="Дата: 01.10.18 Автор: Климов Максим Время: 15:43">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="order-caption-table"><small class="text-muted">Замечания</small></th>
                                            <td class="order-table-font-size"><i class="fas fa-phone-slash" data-toggle="tooltip" data-placement="top" title="87077540021, 877824123483, 877560043234"></i></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-3"></div>
                        </div>
                        <div class="btn-wrapper-open" style="text-align: right;">
                            <!-- <div class="dropdown">
                                <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Позвонить
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="#">877777777</a>
                                    <a class="dropdown-item" href="#">877777777</a>
                                    <a class="dropdown-item" href="#">877777777</a>
                                </div>
                            </div> -->
                            <!--<a href="#" class="btn btn-outline-danger order-button-font-size">Недозвон</a>
                            <a href="#" class="btn btn-outline-secondary order-button-font-size">Приостановить</a> -->
                        </div>
                    </section>
                </section>
            </div>
            <a href="#" class="col-1 order-col-more bg-white" title="Подробнее">
                <table class="order-more-info">
                    <tbody>
                        <tr>
                        <td class="align-middle"><i class="fas fa-angle-right"></i></td>
                        </tr>
                    </tbody>
                </table>
            </a>
        </div>
    </article>
 <?php    
 }
?>
<!-- end Цикл -->


    <article class="order-wrapper border-top border-bottom">
        <div class="row">
            <div class="col-11 order-col-more">
                <!-- Шапка наряда -->
                <section class="order-header border-bottom">
                        <div class="row">
                            <div class="col">
                                <div class="order-date">
                                    <span class="order-day">19</span>
                                    <div class="d-inline-block">
                                        <span class="order-month-year">10.2018</span>
                                        <span class="badge badge-secondary">Пятница</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="order-schedule text-right">
                                    <span><i class="fas fa-bell"></i> 16:00</span>
                                    <span>сегодня</span>
                                </div>
                            </div>
                        </div>
                </section>

                <!-- Контент наряда -->
                <section class="order-body border-bottom">
                    <section class=" ">
                        <div class="row">
                            <div class="col-9">
                                <table class="table order-table">
                                    <tbody>
                                        <tr>
                                            <th><small class="text-muted">Адрес</small></th>
                                            <td><p style="margin: 0;" class="order-hidden-text">11-78-11</p></td>
                                        </tr>
                                        <tr class="order-no-pd-bottom">
                                            <th scope="row" class="pb-0"><small class="text-muted">Причины</small></th>
                                            <td class="order-table-font-size pb-0"><p class="order-hidden-text">Не получает IP</p></td>
                                        </tr>
                                        <tr class="order-comment-wrap">
                                            <th scope="row" class="order-comment px-2 py-0"><small class="text-muted"></small></th>
                                            <td class="order-table-font-size order-comment px-2 py-0"><p class="order-hidden-text order-hidden-comment text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                        </tr>
                                        
                                        <tr>
                                            <th scope="row" class="order-caption-table"><small class="text-muted">Исполнители</small></th>
                                            <td class="order-table-font-size"><p class="order-hidden-text"><i class="fas fa-pause"></i> Климов Максим, Стрижак Татьяна <i class="fas fa-car"></i></p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="order-caption-table order-comment"><small class="text-muted">Модификации</small></th>
                                            <td class="order-table-font-size order-comment"><p class="order-hidden-text order-hidden-comment text-muted" data-toggle="tooltip" data-placement="top" title="Дата: 01.10.18 Автор: Климов Максим Время: 15:43">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="order-caption-table"><small class="text-muted">Замечания</small></th>
                                            <td class="order-table-font-size"><i class="fas fa-phone-slash" data-toggle="tooltip" data-placement="top" title="87077540021, 877824123483, 877560043234"></i></td>
                                            
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-3"></div>
                        </div>
                        <div class="btn-wrapper-open" style="text-align: right;">
                            <!-- <div class="dropdown">
                                <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Позвонить
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="#">877777777</a>
                                    <a class="dropdown-item" href="#">877777777</a>
                                    <a class="dropdown-item" href="#">877777777</a>
                                </div>
                            </div> -->
                            <!--<a href="#" class="btn btn-outline-danger order-button-font-size">Недозвон</a>
                            <a href="#" class="btn btn-outline-secondary order-button-font-size">Приостановить</a> -->
                        </div>
                    </section>
                </section>
            </div>
            <a href="#" class="col-1 order-col-more bg-white" title="Подробнее">
                <table class="order-more-info">
                    <tbody>
                        <tr>
                        <td class="align-middle"><i class="fas fa-angle-right"></i></td>
                        </tr>
                    </tbody>
                </table>
            </a>
        </div>
    </article>
    
    <article class="order-wrapper border-top border-bottom">
        <div class="row">
            <div class="col-11 order-col-more">
                <!-- Шапка наряда -->
                <section class="order-header border-bottom">
                        <div class="row">
                            <div class="col">
                                <div class="order-date">
                                    <span class="order-day">18</span>
                                    <div class="d-inline-block">
                                        <span class="order-month-year">10.2018</span>
                                        <span class="badge badge-danger">Суббота</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="order-schedule text-right">
                                    <span><i class="fas fa-bell"></i> 17:30</span>
                                    <span>завтра</span>
                                </div>
                            </div>
                        </div>
                </section>

                <!-- Контент наряда -->
                <section class="order-body border-bottom">
                    <section class=" ">
                        <div class="row">
                            <div class="col-9">
                                <table class="table order-table">
                                    <tbody>
                                        <tr>
                                            <th><small class="text-muted">Адрес</small></th>
                                            <td><p style="margin: 0;" class="order-hidden-text">11-78-11</p></td>
                                        </tr>
                                        <tr class="order-no-pd-bottom">
                                            <th scope="row" class="pb-0"><small class="text-muted">Причины</small></th>
                                            <td class="order-table-font-size pb-0"><p class="order-hidden-text">Не получает IP</p></td>
                                            
                                        </tr>
                                        <tr class="order-comment-wrap">
                                            <th scope="row" class="order-comment px-2 py-0"><small class="text-muted"></small></th>
                                            <td class="order-table-font-size order-comment px-2 py-0"><p class="order-hidden-text order-hidden-comment text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                        </tr>
                                        
                                        <tr>
                                            <th scope="row" class="order-caption-table"><small class="text-muted">Исполнители</small></th>
                                            <td class="order-table-font-size"><p class="order-hidden-text"><i class="fas fa-pause"></i> Жидко Максим <i class="fas fa-car"></i>, Рыбаков Сергей</p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-3"></div>
                            
                        </div>
                        <div class="btn-wrapper-open" style="text-align: right;">
                            <!-- <a href="#" class="btn btn-outline-success order-button-font-size" data-toggle="modal" data-target="#order-call">Позвонить</a>
                            <a href="#" class="btn btn-outline-danger order-button-font-size">Недозвон</a>
                            <a href="#" class="btn btn-outline-secondary order-button-font-size">Приостановить</a> -->
                        </div>
                    </section>
                </section>
            </div>
            <a href="#" class="col-1 order-col-more bg-white" title="Подробнее">
                <table class="order-more-info">
                    <tbody>
                        <tr>
                        <td class="align-middle"><i class="fas fa-angle-right"></i></td>
                        </tr>
                    </tbody>
                </table>
            </a>
        </div>
    </article>

    <article class="order-wrapper border-top border-bottom">
        <div class="row">
            <div class="col-11 order-col-more">
            <!-- Шапка наряда -->
            <section class="order-header border-bottom">
                    <div class="row">
                        <div class="col">
                            <div class="order-date">
                                <span class="order-day">19</span>
                                <div class="d-inline-block">
                                    <span class="order-month-year">10.2018</span>
                                    <span class="badge badge-danger">Воскресенье</span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="order-schedule text-right">
                                <span><i class="fas fa-bell"></i> 10:05</span>
                                <span>21.10.18</span>
                            </div>
                        </div>
                    </div>
            </section>

            <!-- Контент наряда -->
            <section class="order-body border-bottom">
                <div class="row">
                    <div class="col-9">
                        <table class="table order-table">
                            <tbody>
                                <tr>
                                    <th><small class="text-muted">Адрес</small></th>
                                    <td><p style="margin: 0;" class="order-hidden-text">9-Ардагер-Бутик Стамбул</p></td>
                                </tr>
                                <tr class="order-no-pd-bottom">
                                    <th scope="row" class="pb-0"><small class="text-muted">Причины</small></th>
                                    <td class="order-table-font-size pb-0"><p class="order-hidden-text">Не получает IP</p></td>
                                </tr>
                                <tr class="order-comment-wrap">
                                    <th scope="row" class="order-comment px-2 py-0"><small class="text-muted"></small></th>
                                    <td class="order-table-font-size order-comment px-2 py-0"><p class="order-hidden-text order-hidden-comment text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown </p></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-3"></div>
                </div>
                <div class="btn-wrapper-open" style="text-align: right;">
                    <!-- <a href="#" class="btn btn-outline-success order-button-font-size" data-toggle="modal" data-target="#order-call">Позвонить</a>
                    <a href="#" class="btn btn-outline-danger order-button-font-size">Недозвон</a>
                    <a href="#" class="btn btn-outline-secondary order-button-font-size">Приостановить</a> -->
                </div>
            </section>
        </div>
        <a href="#" class="col-1 order-col-more bg-white">
                <table class="order-more-info" title="Подробнее">
                    <tbody>
                        <tr>
                        <td class="align-middle"><i class="fas fa-angle-right"></i></td>
                        </tr>
                    </tbody>
                </table>
            </a>
    </div>
    </article>
</div>


<!-- Скрипты -->
<script type="text/javascript">
    //Тултип
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    
    //Поля выбора select
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });

    //Отрытие скрытого текста
    $('.order-hidden-text').on('click', function(){
        $(this).toggleClass('order-hidden-text-show');
    })  
</script>
</body>
</html>